import React from "react";

import modalTypes from "../../constants/modalTypes";

const DeleteModalContent = ({ record }) => (
  <div className="default-container">{`Are you sure that you want to delete ${record.title}?`}</div>
);

const InfoModalContent = ({ record }) => (
  <div key={`info-${record.id}`} className="default-container">
    <table border="1" className="full-width">
      <tbody>
        <tr>
          <th>Title</th>
        </tr>
        <tr>
          <td>{record.title}</td>
        </tr>
        <tr>
          <th>Release Year</th>
        </tr>
        <tr>
          <td>{record.releaseYear}</td>
        </tr>
        <tr>
          <th>Format</th>
        </tr>
        <tr>
          <td>{record.format}</td>
        </tr>
        <tr>
          <th>Stars</th>
        </tr>
        <tr>
          <td>{record.stars}</td>
        </tr>
      </tbody>
    </table>
  </div>
);

const ModalContent = ({ type, record }) => {
  switch (type) {
    case modalTypes.delete:
      return <DeleteModalContent record={record} />;
    case modalTypes.info:
      return <InfoModalContent record={record} />;
    default:
      return null;
  }
};

export default ModalContent;
