import React from "react";
import CellHightLighter from "./CellHightLighter";

import { isIncludes } from "../../utilities/helpers";

const maxActorsShown = 3;

const ActorsCell = ({ text, searchText }) => {
  if (text) {
    const actors = text
      .split(", ")
      .filter((actor) => isIncludes(actor, searchText));

    const content =
      actors.length > maxActorsShown
        ? `${actors.slice(0, maxActorsShown).join(", ")}...`
        : actors.join(", ");

    return <CellHightLighter text={content} searchText={searchText} />;
  }

  return text;
};

export default ActorsCell;
