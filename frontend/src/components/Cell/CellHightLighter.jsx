import React from "react";
import Highlighter from "react-highlight-words";

const CellHightLighter = ({ text, searchText }) =>
  !!text && !!searchText ? (
    <Highlighter
      highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
      searchWords={[searchText.toString()]}
      autoEscape
      textToHighlight={text.toString()}
    />
  ) : (
    text
  );

export default CellHightLighter;
