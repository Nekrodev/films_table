import React, { useCallback, useMemo, useState } from "react";
import "antd/dist/antd.css";
import { Table, Input, Button, Space } from "antd";
import {
  SearchOutlined,
  DeleteOutlined,
  InfoOutlined,
} from "@ant-design/icons";

import modalTypes from "../../constants/modalTypes";
import { isIncludes } from "../../utilities/helpers";
import CellHightLighter from "../Cell/CellHightLighter";
import ActorsCell from "../Cell/ActorsCell";

const initialState = {
  searchText: "",
  searchedColumn: "",
  searchInput: null,
};

const FilmsTable = ({ data, showModal }) => {
  const [state, setState] = useState(initialState);

  const handleSearch = useCallback(
    (selectedKeys, confirm, dataIndex) => {
      confirm();
      setState({
        searchText: selectedKeys[0],
        searchedColumn: dataIndex,
      });
    },
    [setState]
  );

  const handleReset = useCallback(
    (clearFilters) => {
      clearFilters();
      setState({ searchText: "" });
    },
    [setState]
  );

  const getColumnSearchProps = useCallback(
    (dataIndex) => ({
      filterDropdown: ({
        setSelectedKeys,
        selectedKeys,
        confirm,
        clearFilters,
      }) => (
        <div className="filter-dropdown">
          <Input
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={(e) =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
          <Space>
            <Button
              type="primary"
              onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
              icon={<SearchOutlined />}
              size="small"
              className="filter-button"
            >
              Search
            </Button>
            <Button
              onClick={() => handleReset(clearFilters)}
              size="small"
              className="filter-button"
            >
              Reset
            </Button>
          </Space>
        </div>
      ),
      filterIcon: (filtered) => (
        <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
      ),
      onFilter: (value, record) => isIncludes(record[dataIndex], value),
      render: (text) => (
        <CellHightLighter
          text={text}
          searchText={
            state.searchedColumn === dataIndex ? state.searchText : ""
          }
        />
      ),
    }),
    [state.searchedColumn, state.searchText, handleReset, handleSearch]
  );

  const columns = useMemo(
    () => [
      {
        title: "ID",
        dataIndex: "id",
        key: "id",
        width: "3em",
        ...getColumnSearchProps("id"),
      },
      {
        title: "Title",
        dataIndex: "title",
        key: "title",
        ...getColumnSearchProps("title"),
        sorter: (a, b) => a.title.localeCompare(b.title),
      },
      {
        title: "Format",
        dataIndex: "format",
        key: "format",
        ...getColumnSearchProps("format"),
        sorter: (a, b) => a.format.localeCompare(b.format),
      },
      {
        title: "Release Year",
        dataIndex: "releaseYear",
        key: "releaseYear",
        ...getColumnSearchProps("releaseYear"),
        sorter: (a, b) => a.releaseYear - b.releaseYear,
      },
      {
        title: "Actors",
        dataIndex: "stars",
        key: "stars",
        width: "20%",
        ...getColumnSearchProps("stars"),
        render: (text) => (
          <ActorsCell
            text={text}
            searchText={
              state.searchedColumn === "stars" ? state.searchText : ""
            }
          />
        ),
      },
      {
        title: "Actions",
        dataIndex: "",
        key: "x",
        render: (text, record) => (
          <div>
            <Button
              className="action-icon"
              icon={<DeleteOutlined />}
              onClick={() => showModal(modalTypes.delete, record)}
            />
            <Button
              className="action-icon"
              icon={<InfoOutlined />}
              onClick={() => showModal(modalTypes.info, record)}
            />
          </div>
        ),
      },
    ],
    [getColumnSearchProps, showModal, state]
  );

  return <Table bordered columns={columns} dataSource={data} rowKey="id" />;
};

export default FilmsTable;
