import React, { useEffect } from "react";
import "antd/dist/antd.css";
import { useDispatch, useSelector } from "react-redux";
import { Modal, Spin } from "antd";

import { getFilmsList, getIsLoading } from "../../reducers";
import useTableModal from "../../hooks/useTableModal";
import FilmsTable from "./Table";
import ModalContent from "../Modals/ModalContent";
import { fetchFilms } from "../../actions/fetchFilms";
import "./../../styles/table.scss";

const ConnectedFilmTable = () => {
  const filmsList = useSelector(getFilmsList);
  const isLoading = useSelector(getIsLoading);
  const { showModal, modalProps, modalContentProps } = useTableModal();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchFilms());
  }, [dispatch]);

  return (
    <Spin spinning={isLoading} delay={300}>
      <FilmsTable data={filmsList} showModal={showModal} />
      <Modal {...modalProps}>
        <ModalContent {...modalContentProps} />
      </Modal>
    </Spin>
  );
};

export default ConnectedFilmTable;
