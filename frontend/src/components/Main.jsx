import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Layout, Menu, Alert } from "antd";

import "antd/dist/antd.css";
import "../styles/main.scss";
import menuKeys from "../constants/mainMenuKeys";
import Root from "./Root";
import { getErrorMessage } from "./../reducers";

const date = new Date();
const { Header, Content, Footer } = Layout;
const { ErrorBoundary } = Alert;

const Main = () => {
  const [selectedMenu, setSelectedMenu] = useState(menuKeys.table);
  const handleSelect = ({ item, key }) => setSelectedMenu(parseInt(key, 10));
  const error = useSelector(getErrorMessage);
  const handleClose = () => {};

  return (
    <Layout className="body">
      <Header className="header">
        <div className="logo" />
        <Menu
          mode="horizontal"
          theme="dark"
          selectedKeys={[selectedMenu]}
          onSelect={handleSelect}
        >
          <Menu.Item
            key={menuKeys.table}
            danger={selectedMenu === menuKeys.table}
          >
            Films Table
          </Menu.Item>
          <Menu.Item key={menuKeys.add} danger={selectedMenu === menuKeys.add}>
            Add Film
          </Menu.Item>
        </Menu>
      </Header>
      <Content className="site-layout">
        <div className="site-layout-background">
          <ErrorBoundary>
            {!!error && (
              <Alert
                message="Something went wrong..."
                description={error}
                type="error"
                closable
                onClose={handleClose}
              />
            )}
            <Root route={selectedMenu} />
          </ErrorBoundary>
        </div>
      </Content>
      <Footer className="text-centred">{`©${date.getFullYear()} Created by Nekrodev`}</Footer>
    </Layout>
  );
};

export default Main;
