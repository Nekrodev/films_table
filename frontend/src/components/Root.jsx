import React from "react";

import menuKeys from "../constants/mainMenuKeys";
import AddFilms from "./AddFilms";
import FilmsTable from "./FilmsTable";

const Root = ({ route }) => {
  switch (route) {
    case menuKeys.add:
      return <AddFilms />;
    case menuKeys.table:
      return <FilmsTable />;
    default:
      return <div>Page not Found</div>;
  }
};

export default Root;
