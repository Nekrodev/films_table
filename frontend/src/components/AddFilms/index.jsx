import React from "react";
import { Row, Col, Spin } from "antd";
import { useDispatch, useSelector } from "react-redux";

import { addFilm } from "../../actions/addFilm";
import { getIsLoading } from "../../reducers";
import AddFilmForm from "./AddFilmForm";
import UploadFilms from "./UploadFilms";
import "./../../styles/add-films.scss";

const AddFilms = () => {
  const dispatch = useDispatch();
  const handleFinish = (values) => dispatch(addFilm(values));
  const isLoading = useSelector(getIsLoading);

  return (
    <>
      <div>Add film manually by filling the from, or load txt file!</div>
      <Row gutter={[16, 16]} style={{ margin: 0 }}>
        <Col xs={24} sm={24} md={12} lg={12} xl={12} className="blue-column ">
          <Spin spinning={isLoading}>
            <AddFilmForm handleFinish={handleFinish} />
          </Spin>
        </Col>
        <Col xs={24} sm={24} md={3} lg={3} xl={3}>
          <Row justify="center" align="middle" className="or">
            OR
          </Row>
        </Col>
        <Col xs={24} sm={24} md={9} lg={9} xl={9} className="blue-column ">
          <Row justify="center" align="middle" className="full-height">
            <UploadFilms />
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default AddFilms;
