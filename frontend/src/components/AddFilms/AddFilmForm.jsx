import "antd/dist/antd.css";
import { Form, Input, Button, InputNumber, Select } from "antd";

import { formats } from "./../../constants/film";
import { uniqueAuthorValidator, yearValidator } from "./../../utilities/validators";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const AddFilmForm = ({ handleFinish, handleFinishFailed }) => {
  return (
    <Form
      {...layout}
      name="basic"
      className="default-container"
      onFinish={handleFinish}
      onFinishFailed={handleFinishFailed}
    >
      <Form.Item
        label="Title"
        name="title"
        rules={[
          {
            required: true,
            message: "Please input film title!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Release Year"
        name="releaseYear"
        rules={[
          {
            required: true,
            message: "Please input Realise Year!",
          },
          {
            validator: yearValidator,
          },
        ]}
      >
        <InputNumber />
      </Form.Item>

      <Form.Item
        label="Format"
        name="format"
        rules={[
          {
            required: true,
            message: "Please input format!",
          },
        ]}
      >
        <Select>
          {formats.map((format) => (
            <Select.Option value={format} key={format}>
              {format}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        label="Stars"
        name="stars"
        rules={[
          {
            required: true,
            message: "Please input known film stars!",
          },
          {
            validator: uniqueAuthorValidator,
          },
        ]}
      >
        <Input placeholder={"Ivan Ivanov, David Garet, ect"} />
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default AddFilmForm;
