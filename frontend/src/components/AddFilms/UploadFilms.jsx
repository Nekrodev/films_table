import React, { useState } from "react";
import "antd/dist/antd.css";
import { Upload, Button, message, Col } from "antd";
import { useDispatch } from "react-redux";
import { UploadOutlined } from "@ant-design/icons";

import { uploadFilms } from "../../actions/uploadFilms";

const initialState = {
  file: null,
  uploading: false,
};

const UploadFilms = () => {
  const [state, setState] = useState(initialState);
  const dispatch = useDispatch();
  const { uploading, file } = state;

  const handleSuccess = () => {
    setState(initialState);
    message.success("Upload successfully.");
  };

  const handleError = () => {
    setState({
      file: state.file,
      uploading: false,
    });
  };

  const handleRemove = (file) => {
    setState(initialState);
  };

  const handleUpload = () => {
    const formData = new FormData();
    formData.append("file", file);

    setState({
      uploading: true,
      file: state.file,
    });

    dispatch(
      uploadFilms({
        data: formData,
        success: handleSuccess,
        error: handleError,
      })
    );
  };

  const beforeUpload = (file) => {
    setState({
      file,
      uploading: state.uploading,
    });

    return false;
  };

  return (
    <Col flex={true} className="default-container">
      <Upload
        onRemove={handleRemove}
        beforeUpload={beforeUpload}
        fileList={file ? [file] : file}
        accept=".txt"
      >
        <Button icon={<UploadOutlined />} className="upload-button">
          Select File
        </Button>
      </Upload>
      <Button
        type="primary"
        onClick={handleUpload}
        disabled={!state.file}
        loading={uploading}
        className="upload-button submit"
      >
        {uploading ? "Uploading" : "Start Upload"}
      </Button>
    </Col>
  );
};

export default UploadFilms;
