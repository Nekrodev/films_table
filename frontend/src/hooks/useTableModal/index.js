import { useCallback, useMemo, useState } from "react";
import { useDispatch } from "react-redux";

import { deleteFilm } from "../../actions/deleteFilm";
import modalTypes from "../../constants/modalTypes";

const initialState = { type: "", record: null };

export default function useTableModal() {
  const [state, setState] = useState({
    type: "",
    record: null,
    visible: false,
  });
  const dispatch = useDispatch();
  const showModal = useCallback(
    (type, record) => {
      setState({
        type,
        record,
        visible: !!type && !!record,
      });
    },
    [setState]
  );

  const hideModal = useCallback(() => setState({ ...state, visible: false }), [
    setState,
    state,
  ]);

  const handleDelete = useCallback(
    (id) => {
      dispatch(deleteFilm(id));
    },
    [dispatch]
  );

  const handleOk = useCallback(
    (id) => {
      if (state.type === modalTypes.delete) {
        handleDelete(id);
      }

      setState(initialState);
    },
    [handleDelete, setState, state.type]
  );

  const modalProps = useMemo(
    () => ({
      visible: state.visible,
      onOk: () => handleOk(state.record && state.record.id),
      onCancel: hideModal,
    }),
    [state, handleOk, hideModal]
  );

  return {
    showModal,
    modalProps,
    modalContentProps: {
      type: state.type,
      record: state.record,
    },
  };
}
