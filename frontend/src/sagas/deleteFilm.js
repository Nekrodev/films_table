import { call, takeLatest, put, all } from "redux-saga/effects";
import { message } from "antd";

import { filmDeleted, filmDeletionFailed } from "../actions/deleteFilm";
import { DELETE_FILM, FILM_DELETED } from "../constants/actions";
import deleteFilm from "./../services/deleteFilm";
import { fetchFilms } from "./../actions/fetchFilms";
import { getErrorMessage } from "./../utilities/helpers";

function* deleteFilmSaga(action) {
  try {
    yield call(deleteFilm, action.payload.id);
    yield put(filmDeleted(action.payload.id));
    message.success("Deleted successfully.");
  } catch (e) {
    yield put(filmDeletionFailed(getErrorMessage(e)));
  }
}

function* updateDataOnDeletion() {
  yield put(fetchFilms());
}

function* watchDeleteFilm() {
  yield all([
    yield takeLatest(DELETE_FILM, deleteFilmSaga),
    yield takeLatest(FILM_DELETED, updateDataOnDeletion),
  ]);
}

export default watchDeleteFilm;
