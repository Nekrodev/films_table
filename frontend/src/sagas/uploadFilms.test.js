import SagaTester from "redux-saga-tester";
import reducer, { INITIAL_STATE } from "./../reducers";
import uploadFilmsSaga from "./uploadFilms";
import testData from "./../constants/data";
import * as FilmActions from "./../constants/actions";
import Axios from "axios";
import { uploadFilms } from "../actions/uploadFilms";

jest.mock("axios");

test("upload films successfully", async () => {
  const sagaTester = new SagaTester({
    initialState: INITIAL_STATE,
    reducers: reducer,
  });

  const resp = { message: "success" };
  Axios.post.mockResolvedValue(resp);

  sagaTester.start(uploadFilmsSaga);

  sagaTester.dispatch(
    uploadFilms({ data: testData, success: () => {}, error: () => {} })
  );
  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    isLoading: true,
  });

  await sagaTester.waitFor(FilmActions.FILMS_UPLOADED);

  expect(sagaTester.wasCalled(FilmActions.FILMS_UPLOADED)).toBeTruthy();

  expect(sagaTester.getState()).toStrictEqual(INITIAL_STATE);
});

test("upload film not successfully", async () => {
  const sagaTester = new SagaTester({
    initialState: INITIAL_STATE,
    reducers: reducer,
  });

  Axios.post.mockRejectedValueOnce({ message: "error" });

  sagaTester.start(uploadFilmsSaga);

  sagaTester.dispatch(
    uploadFilms({ data: testData, success: () => {}, error: () => {} })
  );

  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    isLoading: true,
  });

  await sagaTester.waitFor(FilmActions.FILMS_REQUEST_FAILED);

  expect(sagaTester.wasCalled(FilmActions.FILMS_REQUEST_FAILED)).toBeTruthy();

  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    error: "error",
  });
});
