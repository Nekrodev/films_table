import SagaTester from "redux-saga-tester";
import { addFilm } from "../actions/addFilm";
import reducer, { INITIAL_STATE } from "./../reducers";
import addFilmSaga from "./addFilm";
import testData from "./../constants/data";
import * as FilmActions from "./../constants/actions";
import Axios from "axios";

const testFilm = testData[0];

jest.mock("axios");

test("add film successfully", async () => {
  const sagaTester = new SagaTester({
    initialState: INITIAL_STATE,
    reducers: reducer,
  });

  const resp = { data: "success" };
  Axios.post.mockResolvedValue(resp);

  sagaTester.start(addFilmSaga);

  sagaTester.dispatch(addFilm(testFilm));
  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    isLoading: true,
  });
  await sagaTester.waitFor(FilmActions.FILM_ADDED);

  expect(sagaTester.wasCalled(FilmActions.FILM_ADDED)).toBeTruthy();

  expect(sagaTester.getState()).toStrictEqual(INITIAL_STATE);
});

test("add film not successfully", async () => {
  const sagaTester = new SagaTester({
    initialState: INITIAL_STATE,
    reducers: reducer,
  });

  Axios.post.mockRejectedValueOnce({ message: "error" });

  sagaTester.start(addFilmSaga);

  sagaTester.dispatch(addFilm(testFilm));
  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    isLoading: true,
  });

  await sagaTester.waitFor(FilmActions.FILMS_REQUEST_FAILED);

  expect(sagaTester.wasCalled(FilmActions.FILMS_REQUEST_FAILED)).toBeTruthy();

  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    error: "error",
  });
});
