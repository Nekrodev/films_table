import { call, takeLatest, put } from "redux-saga/effects";

import { FETCH_FILMS } from "../constants/actions";
import { filmsLoaded, filmsFetchingFailed } from "./../actions/fetchFilms";
import fetchFilms from "./../services/fetchFilms";
import { getErrorMessage } from "./../utilities/helpers";

function* fetchFilmsSaga() {
  try {
    const response = yield call(fetchFilms);
    yield put(filmsLoaded(response.data.films));
  } catch (e) {
    yield put(filmsFetchingFailed(getErrorMessage(e)));
  }
}

function* watchFetchFilms() {
  yield takeLatest(FETCH_FILMS, fetchFilmsSaga);
}

export default watchFetchFilms;
