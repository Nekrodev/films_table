import { all } from "redux-saga/effects";

import watchFetchFilms from "./fetchFilm";
import watchDeleteFilm from "./deleteFilm";
import watchUploadFilms from "./uploadFilms";
import watchAddFilm from "./addFilm";

export default function* rootSaga() {
  yield all([
    watchFetchFilms(),
    watchDeleteFilm(),
    watchUploadFilms(),
    watchAddFilm()
  ]);
}
