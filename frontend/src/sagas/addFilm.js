import { call, takeLatest, put } from "redux-saga/effects";
import { message } from "antd";

import { ADD_FILM } from "../constants/actions";
import addFilm from "../services/addFilm";
import { filmAdded, filmAdditionFailed } from "../actions/addFilm";
import { getErrorMessage } from "./../utilities/helpers";

function* addFilmsSaga(action) {
  try {
    const data = action.payload;
    yield call(addFilm, data);
    yield put(filmAdded(data));
    message.success("Upload successfully.");
  } catch (e) {
    yield put(filmAdditionFailed(getErrorMessage(e)));
  }
}

function* watchAddFilm() {
  yield takeLatest(ADD_FILM, addFilmsSaga);
}

export default watchAddFilm;
