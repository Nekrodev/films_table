import { call, takeLatest, put } from "redux-saga/effects";

import { UPLOAD_FILMS } from "../constants/actions";
import uploadFilms from "../services/uploadFilms";
import { filmsUploaded, filmsUploadingFailed } from "../actions/uploadFilms";
import { getErrorMessage } from "./../utilities/helpers";

function* uploadFilmsSaga(action) {
  const { data, success, error } = action.payload;

  try {
    yield call(uploadFilms, data);
    success();
    yield put(filmsUploaded());
  } catch (e) {
    error();
    yield put(filmsUploadingFailed(getErrorMessage(e)));
  }
}

function* watchUploadFilms() {
  yield takeLatest(UPLOAD_FILMS, uploadFilmsSaga);
}

export default watchUploadFilms;