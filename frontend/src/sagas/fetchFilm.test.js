import SagaTester from "redux-saga-tester";
import reducer, { INITIAL_STATE } from "./../reducers";
import fetchFilmSaga from "./fetchFilm";
import testData from "./../constants/data";
import * as FilmActions from "./../constants/actions";
import Axios from "axios";
import { fetchFilms } from "../actions/fetchFilms";

jest.mock("axios");

test("fetch film successfully", async () => {
  const sagaTester = new SagaTester({
    initialState: INITIAL_STATE,
    reducers: reducer,
  });

  const resp = { data: { films: testData } };
  Axios.get.mockResolvedValue(resp);

  sagaTester.start(fetchFilmSaga);

  sagaTester.dispatch(fetchFilms());
  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    isLoading: true,
  });

  await sagaTester.waitFor(FilmActions.FILMS_LOADED);

  expect(sagaTester.wasCalled(FilmActions.FILMS_LOADED)).toBeTruthy();

  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    films: testData,
  });
});

test("fetch film not successfully", async () => {
  const sagaTester = new SagaTester({
    initialState: INITIAL_STATE,
    reducers: reducer,
  });

  Axios.get.mockRejectedValueOnce({ message: "error" });

  sagaTester.start(fetchFilmSaga);

  sagaTester.dispatch(fetchFilms());
  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    isLoading: true,
  });

  await sagaTester.waitFor(FilmActions.FILMS_REQUEST_FAILED);

  expect(sagaTester.wasCalled(FilmActions.FILMS_REQUEST_FAILED)).toBeTruthy();

  expect(sagaTester.getState()).toStrictEqual({
    ...INITIAL_STATE,
    error: "error",
  });
});
