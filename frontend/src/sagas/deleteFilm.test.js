import SagaTester from "redux-saga-tester";
import { deleteFilm } from "../actions/deleteFilm";
import reducer, { INITIAL_STATE } from "./../reducers";
import deleteFilmSaga from "./deleteFilm";
import testData from "./../constants/data";
import * as FilmActions from "./../constants/actions";
import Axios from "axios";

const initialState = {
  ...INITIAL_STATE,
  films: testData,
}

jest.mock("axios");

test("delete film successfully", async () => {
  const sagaTester = new SagaTester({
    initialState,
    reducers: reducer,
  });

  const resp = { data: "success" };
  Axios.delete.mockResolvedValue(resp);

  sagaTester.start(deleteFilmSaga);

  sagaTester.dispatch(deleteFilm(1));
  expect(sagaTester.getState()).toStrictEqual({
    ...initialState,
    isLoading: true,
  });

  await sagaTester.waitFor(FilmActions.FILM_DELETED);

  expect(sagaTester.wasCalled(FilmActions.FILM_DELETED)).toBeTruthy();
  expect(sagaTester.wasCalled(FilmActions.FETCH_FILMS)).toBeTruthy();

  expect(sagaTester.getState()).toStrictEqual({
    ...initialState,
    isLoading: true,
  });

});

test("delete film not successfully", async () => {
  const sagaTester = new SagaTester({
    initialState,
    reducers: reducer,
  });

  Axios.delete.mockRejectedValueOnce({ message: "error" });

  sagaTester.start(deleteFilmSaga);

  sagaTester.dispatch(deleteFilm(1));
  expect(sagaTester.getState()).toStrictEqual({
    ...initialState,
    isLoading: true,
  });

  await sagaTester.waitFor(FilmActions.FILMS_REQUEST_FAILED);

  expect(sagaTester.wasCalled(FilmActions.FILMS_REQUEST_FAILED)).toBeTruthy();

  expect(sagaTester.getState()).toStrictEqual({
    ...initialState,
    error: "error"
  });

});
