export const FETCH_FILMS = "films/FETCH_FILMS";
export const FILMS_LOADED = "films/FILMS_LOADED";

export const DELETE_FILM = "films/DELETE_FILM";
export const FILM_DELETED = "films/FILM_DELETED";

export const ADD_FILM = "films/ADD_FILM";
export const FILM_ADDED = "films/FILM_ADDED";

export const UPLOAD_FILMS = "films/UPLOAD_FILMS";
export const FILMS_UPLOADED = "films/FILMS_UPLOADED";

export const FILMS_REQUEST_FAILED = "films/FILMS_REQUEST_FAILED";