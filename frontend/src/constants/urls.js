/* eslint-disable import/no-anonymous-default-export */
export default {
  fetchFilms: "/films",
  addFilm: "/film/add",
  deleteFilm: "/film/delete",
  uploadFile: "/upload"
}