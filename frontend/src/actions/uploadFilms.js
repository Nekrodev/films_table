import * as filmsActions from "../constants/actions";

export const uploadFilms = (data) => ({
  type: filmsActions.UPLOAD_FILMS,
  payload: data,
});

export const filmsUploadingFailed = (error) => ({
  type: filmsActions.FILMS_REQUEST_FAILED,
  error,
});

export const filmsUploaded = () => ({
  type: filmsActions.FILMS_UPLOADED,
});
