import * as filmsActions from "./../constants/actions";

export const addFilm = (filmDescription) => ({
  type: filmsActions.ADD_FILM,
  payload: filmDescription,
});

export const filmAdditionFailed = (error) => ({
  type: filmsActions.FILMS_REQUEST_FAILED,
  error,
});

export const filmAdded = (filmDescription) => ({
  type: filmsActions.FILM_ADDED,
  payload: filmDescription
});