import * as filmsActions from "./../constants/actions";

export const deleteFilm = (id) => ({
  type: filmsActions.DELETE_FILM,
  payload: { id },
});

export const filmDeletionFailed = (error) => ({
  type: filmsActions.FILMS_REQUEST_FAILED,
  error,
});

export const filmDeleted = (id) => ({
  type: filmsActions.FILM_DELETED,
  payload: { id },
});
