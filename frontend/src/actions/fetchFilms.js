import * as filmsActions from "./../constants/actions";

export const fetchFilms = () => ({
  type: filmsActions.FETCH_FILMS,
});

export const filmsFetchingFailed = (error) => ({
  type: filmsActions.FILMS_REQUEST_FAILED,
  error
});

export const filmsLoaded = (films) => ({
  type: filmsActions.FILMS_LOADED,
  payload: { films }
});
