import Axios from "axios";

import urls from "./../constants/urls";

async function deleteFilm(id) {
  return await Axios.delete(urls.deleteFilm, { data: { id } });
}

export default deleteFilm;
