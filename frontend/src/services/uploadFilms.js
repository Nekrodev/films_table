import Axios from "axios";

import urls from "./../constants/urls";

async function uploadFilms(filmData) {
  return await Axios.post(urls.uploadFile, filmData);
}

export default uploadFilms;
