import Axios from "axios";

import urls from "./../constants/urls";

async function fetchFilms() {
  return await Axios.get(urls.fetchFilms);
}

export default fetchFilms;
