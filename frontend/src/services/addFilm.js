import Axios from "axios";

import urls from "./../constants/urls";

async function addFilm(data) {
  return await Axios.post(urls.addFilm, data);
}

export default addFilm;
