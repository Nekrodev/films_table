import { combineReducers } from "redux";

import * as filmActions from "./../constants/actions";

export const INITIAL_STATE = {
  films: [],
  error: null,
  isLoading: false,
};

const films = (state = INITIAL_STATE.films, action) => {
  switch (action.type) {
    case filmActions.FILMS_LOADED:
      return action.payload.films;
    default:
      return state;
  }
};

const error = (state = INITIAL_STATE.error, action) => {
  switch (action.type) {
    case filmActions.FILMS_REQUEST_FAILED: {
      return action.error;
    }
    case filmActions.ADD_FILM:
    case filmActions.DELETE_FILM:
    case filmActions.FETCH_FILMS:
    case filmActions.FILMS_LOADED:
    case filmActions.FILM_ADDED:
    case filmActions.FILM_DELETED:
      return null;
    default:
      return state;
  }
};

const isLoading = (state = INITIAL_STATE.isLoading, action) => {
  switch (action.type) {
    case filmActions.ADD_FILM:
    case filmActions.DELETE_FILM:
    case filmActions.UPLOAD_FILMS:
    case filmActions.FETCH_FILMS: {
      return true;
    }
    case filmActions.FILMS_LOADED:
    case filmActions.FILMS_UPLOADED: 
    case filmActions.FILM_ADDED:
    case filmActions.FILM_DELETED:
    case filmActions.FILMS_REQUEST_FAILED: {
      return false;
    }
    default:
      return state;
  }
};

export const getFilmsList = (state) => state.films;
export const getIsLoading = (state) => state.isLoading;
export const getErrorMessage = (state) => state.error;

export default combineReducers({
  films,
  error,
  isLoading,
});
