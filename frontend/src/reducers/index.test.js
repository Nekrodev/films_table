import { addFilm } from "../actions/addFilm";
import { deleteFilm } from "../actions/deleteFilm";
import {
  fetchFilms,
  filmsFetchingFailed,
  filmsLoaded,
} from "../actions/fetchFilms";
import { uploadFilms } from "../actions/uploadFilms";
import testData from "./../constants/data";
import reducer, { INITIAL_STATE } from "./index";

const emptyAction = { type: "" };

test("reducer inits", () => {
  expect(reducer(INITIAL_STATE, emptyAction)).toStrictEqual(INITIAL_STATE);
});

test("films fetches successfully", () => {
  let state = reducer(INITIAL_STATE, emptyAction);
  expect(state).toBe(INITIAL_STATE);

  state = reducer(state, fetchFilms());

  expect(state).toStrictEqual({
    films: [],
    error: null,
    isLoading: true,
  });

  state = reducer(state, filmsLoaded(testData));

  expect(state).toStrictEqual({
    films: testData,
    error: null,
    isLoading: false,
  });
});

test("films fetches unsuccessfully", () => {
  let state = reducer(INITIAL_STATE, emptyAction);
  expect(state).toBe(INITIAL_STATE);

  state = reducer(state, fetchFilms());

  expect(state).toStrictEqual({
    films: [],
    error: null,
    isLoading: true,
  });

  const err = "some error";
  state = reducer(state, filmsFetchingFailed(err));

  expect(state).toStrictEqual({
    films: [],
    error: err,
    isLoading: false,
  });
});

test("action starts loading", () => {
  expect(reducer(INITIAL_STATE, uploadFilms({}))).toStrictEqual({
    films: [],
    error: null,
    isLoading: true,
  });

  expect(reducer(INITIAL_STATE, deleteFilm({}))).toStrictEqual({
    films: [],
    error: null,
    isLoading: true,
  });

  expect(reducer(INITIAL_STATE, addFilm({}))).toStrictEqual({
    films: [],
    error: null,
    isLoading: true,
  });
});
