import { arrayUnique } from "./helpers";
import { firstFilmYear } from "./../constants/film";

const nextYear = new Date().getFullYear() + 1;

export const yearValidator = (_, value) =>
  value && parseInt(value, 10) > firstFilmYear && parseInt(value, 10) < nextYear
    ? Promise.resolve()
    : Promise.reject(
        `Year must be greater then ${firstFilmYear} and less then ${nextYear}!`
      );

export const uniqueAuthorValidator = (_, value) => {
  const actors = value ? value.split(",").map((x) => x.trim()) : [];
  return arrayUnique(actors).length === actors.length
    ? Promise.resolve()
    : Promise.reject(`Please insert unique authors!`);
};
