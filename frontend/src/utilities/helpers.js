export const isIncludes = (text, searchText) =>
  !!text &&
  text.toString().toLowerCase().includes(searchText.toString().toLowerCase());

export const getErrorMessage = (e) => {
  if (e.response && e.response.data && e.response.data.message) {
    return e.response.data.message;
  }

  return e.message;
};

export const arrayUnique = (arr) =>
  arr.filter((value, index, self) => self.indexOf(value) === index);
