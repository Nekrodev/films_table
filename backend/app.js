const express = require("express");
const initRoutes = require("./src/routes");
const { initDB } = require("./src/services/initDbConnection");
const bodyParser = require("body-parser");
const path = require('path');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "build")));
app.use(bodyParser.json());

initDB();
initRoutes(app);

app.listen(8080, () => {
  console.log("server listening port: 8080");
});
