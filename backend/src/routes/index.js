const express = require("express");
const upload = require("../controllers/upload.controller");
const filmController = require("../controllers/film.controller");
const { body } = require("express-validator");

const routes = (app) => {
  const router = express.Router();

  router.post("/upload", upload);

  router.get("/films", filmController.getFilms);

  router.post(
    "/film/add",
    [
      body("title").isString(),
      body("releaseYear").isInt(),
      body("format").isString(),
      body("stars").isString(),
    ],
    filmController.addFilm
  );

  router.delete("/film/delete", filmController.deleteFilm);
  
  router.get("/", function (req, res) {
    res.sendFile(path.join(__dirname, "build", "index.html"));
  });

  app.use(router);
};

module.exports = routes;
