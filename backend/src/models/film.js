const { Model, DataTypes, Sequelize } = require("sequelize");
const { sequelize } = require("./../services/initDbConnection");

class Film extends Model {}

Film.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    releaseYear: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    format: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stars: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    modelName: "film",
    indexes: [
      {
        unique: true,
        fields: ["title", "releaseYear", "format"],
      },
    ],
  }
);

module.exports = Film;
