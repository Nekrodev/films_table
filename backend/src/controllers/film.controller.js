const { validationResult } = require("express-validator");
const addFilmToBase = require("./../services/addFilm");
const getFilmsList = require("./../services/getFilmsList");
const deleteFilmFromBase = require("./../services/deleteFilm");
const { UniqueConstraintError } = require("sequelize");

async function getFilms(req, res) {
  try {
    const films = await getFilmsList();
    res.status(200).send({ films });
  } catch (err) {
    res.status(500).send({
      message: err.message,
    });
  }
}

async function deleteFilm(req, res) {
  try {
    await deleteFilmFromBase(req.body.id);
    res.status(200).send({
      message: "File deleted successfully.",
    });
  } catch (err) {
    res.status(500).send({
      message: err.message,
    });
  }
}

async function addFilm(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    await addFilmToBase(req.body);
    res.status(200).send();
  } catch (err) {
    if (err instanceof UniqueConstraintError) {
      res.status(500).send({
        message: `Duplication detected, film already exists.`,
      });
    } else {
      res.status(500).send({
        message: err.message,
      });
    }
  }
}

module.exports = {
  getFilms,
  deleteFilm,
  addFilm,
};
