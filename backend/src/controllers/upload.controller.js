const multiparty = require("multiparty");
const { UniqueConstraintError } = require("sequelize");
const addFilesFromFile = require("./../services/addFilesFromFile");

const upload = (req, res) => {
  const form = new multiparty.Form();
  form.parse(req, function (err, fields, files) {
    if (files.file.length < 1) {
      res.status(400).send({ message: "Please upload a file!" });
    }

    addFilesFromFile(files.file[0])
      .then(() => {
        res.status(200).send({
          message: "File uploaded successfully",
        });
      })
      .catch((err) => {
        if (err instanceof UniqueConstraintError) {
          res.status(500).send({
            message: `Could not upload the file. Duplication detected!`,
          });
        } else {
          res.status(500).send({
            message: `Could not upload the file. ${err}`,
          });
        }
      });
  });
};

module.exports = upload;
