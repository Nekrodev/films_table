const permittedUploadExtensions = [".txt"];

module.exports = {
  permittedUploadExtensions
}