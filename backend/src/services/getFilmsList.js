const Film = require("../models/film");

async function getFilmsList() {
  return (await Film.findAll()).map((film) => ({
    id: film.id,
    title: film.title,
    releaseYear: film.releaseYear,
    format: film.format,
    stars: film.stars
  }));
}

module.exports = getFilmsList;
