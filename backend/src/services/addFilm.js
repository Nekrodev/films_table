const Film = require("../models/film");

async function addFilmToBase(film) {
  return await Film.create({
    title: film.title,
    releaseYear: film.releaseYear,
    format: film.format,
    stars: film.stars,
  });
}

module.exports = addFilmToBase;
