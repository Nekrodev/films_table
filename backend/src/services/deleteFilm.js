const Film = require("../models/film");

async function deleteFilmFromBase(id) {
    return await Film.destroy({
      where: {
        id: id,
      },
    });
}

module.exports = deleteFilmFromBase;
