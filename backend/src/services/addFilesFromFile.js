const fs = require("fs");
const path = require("path");
const { permittedUploadExtensions } = require("./../constants");
const Film = require("../models/film");

const isFilm = (data) =>
  data &&
  data.title &&
  typeof data.title === "string" &&
  data.stars &&
  typeof data.stars === "string" &&
  data.format &&
  typeof data.format === "string" &&
  data.releaseYear &&
  typeof data.releaseYear === "number";

const parseFile = (file) => {
  const lines = fs.readFileSync(file.path, "utf8").toString().split("\n");
  let current = {};
  const films = [];
  lines.forEach((line) => {
    if (line.trim() === "") {
      return;
    }

    const parts = line.split(":");
    const key = parts[0].toLowerCase().trim();
    if (key === "title") {
      current = {};
      current.title = parts[1].trim();
    }

    if (key === "stars") {
      current.stars = parts[1].trim();
      films.push(current);
    }

    if (key === "release year") {
      current.releaseYear = parseInt(parts[1].trim(), 10);
    }

    if (key === "format") {
      current.format = parts[1].trim();
    }
  });

  if (
    films.length > 0 &&
    films.reduce((prev, curr) => !!prev && isFilm(curr), true)
  ) {
    return films;
  }

  throw Error("Could not parse file!");
};

async function addFilesFromFile(file) {
  if (!permittedUploadExtensions.includes(path.extname(file.path))) {
    throw Error("Wrong file extension, please use .txt!");
  }

  return await Film.bulkCreate(parseFile(file));
}

module.exports = addFilesFromFile;
