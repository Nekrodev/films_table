const Sequelize = require("sequelize");

const sequelize = new Sequelize("filmdb", "root", "secure_key", {
  dialect: "mysql",
  host: "db",
  define: {
    timestamps: false,
  },
});

const initDB = () => {
  sequelize
    .sync()
    .then(() => console.log("DB connection established!"))
    .catch((err) => {
      console.log(err);
      setTimeout(initDB, 5000);
    });
};

module.exports = { initDB, sequelize };
