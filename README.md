# README

### What is this repository for?

- Films Table example project
- 1.0.0

### What do you need to set up?

- Docker
- Node js

## Technology overview:

### Fronted:

- React JS
- Redux
- Redux-Saga

### Backend:

- NodeJS
- Express

### MySQL

### Fist setup:

1. Clone repository to your local machine
2. in `./frontend` run: **npm install**
3. in `./backend` run: **npm install**
4. go to `./` run: **docker-compose build**
5. run: **docker-compose up**

### Build frontend:
1. go to `./frontend` run: **npm run-script build**, this command will create build folder.
2. Copy `build` folder to the `./backend` after that all application will be accessible on localhost:8080

### Backend development:

- In order to apply your backend changes, run:

1. go to `./` run: **docker-compose build**
2. run: **docker-compose up**

### Fronted development:
1. go to `./frontend` run: **npm start**

## Run test:

### Frontend:
1. go to `./frontend` run: **npm test**

### Contacts

- If you have any questions, please send me an email: **timvd152@gmail.com**
